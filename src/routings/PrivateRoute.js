import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ path, component: Component , authenticationReducer, ...props}) => (
  <Route path={path} render={(props) => {
    console.log(authenticationReducer);
    if (authenticationReducer["isLoggedIn"] === true) {
      return <Component {...props} />
    } else {
      return <Redirect to="/login" />
    }
  }} {...props}/>
);

const mapStateToProps = state => {
  return {
    authenticationReducer: state.authenticationReducer
  }
}

export default connect(mapStateToProps) (PrivateRoute);