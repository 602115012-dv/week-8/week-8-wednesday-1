import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import NavBar from '../components/NavBar';
import WrappedLoginForm from '../components/LoginForm';
import UserList from '../components/UserList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logIn, logOut } from '../actions/authenticationActions';
import UserTaskList from '../components/UserTaskList';
import PrivateRoute from './PrivateRoute';

const MainRouting = (props) => {
  const { logIn, logOut } = props;
  return (
    <BrowserRouter>
      <Route exact path="/" render={() => {
        return <Redirect to="/users"/>
      }}/>
      <Route path="/processLogin/:username&:password" render={(props) => {
        logIn(props.match.params["username"], props.match.params["password"]);
        return <Redirect to="/users" />
      }} />
      <Route path="/processLogout" render={() => {
        logOut();
        return <Redirect to="/" />
      }} />
      <Route path="/" component={NavBar} />
      <Route path="/login" component={WrappedLoginForm} />
      <PrivateRoute exact={true} path="/users" component={UserList} />
      <PrivateRoute path="/users/:userId/todo" component={UserTaskList} />
    </BrowserRouter>
  );
}

const mapStateToProps = state => {
  return {
    authenticationReducer: state.authenticationReducer
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ logIn, logOut } , dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (MainRouting);