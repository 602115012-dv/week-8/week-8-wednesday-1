export const LOG_IN = "LOG_IN";
export const LOGOUT = "LOGOUT";

export const logIn = (username, password) => {
  return {
    type: LOG_IN,
    username,
    password
  }
}

export const logOut = () => {
  return {
    type: LOGOUT,
  }
}