export const FETCH_TASKS_BEGIN = "FETCH_TASKS_BEGIN";
export const FETCH_TASKS_SUCCEED = "FETCH_TASKS_SUCCEED";
export const FETCH_TASKS_FAIL = "FETCH_TASKS_FAIL";
export const SET_TASK_STATUS_FILTER_VALUE = "SET_TASK_STATUS_FILTER_VALUE";
export const SET_TASK_TITLE_FILTER_VALUE = "SET_TASK_TITLE_FILTER_VALUE";
export const UPDATE_TASKS = "UPDATE_TASKS";

export const fetchTasksBegin = () => (
  {
    type: FETCH_TASKS_BEGIN
  }
)

export const fetchTasksSucceed = tasks => (
  {
    type: FETCH_TASKS_SUCCEED,
    payload: tasks
  }
)

export const fetchTasksFail = error => (
  {
    type: FETCH_TASKS_FAIL,
    payload: error
  }
)

export const setTaskStatusFilterValue = taskStatusFilterValue => (
  {
    type: SET_TASK_STATUS_FILTER_VALUE,
    payload: taskStatusFilterValue
  }
)

export const  setTaskTitleFilterValue = taskTitleFilterValue => (
  {
    type: SET_TASK_TITLE_FILTER_VALUE,
    payload: taskTitleFilterValue
  }
)

export const updateTasks = tasks => (
  {
    type: UPDATE_TASKS,
    payload: tasks
  }
)


export const fetchUserTasks = userId => {
  return dispatch => {
    dispatch(fetchTasksBegin());
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`)
        .then(res => res.json())
        .then(data => dispatch(fetchTasksSucceed(data)))
        .catch(err => dispatch(fetchTasksFail(err)));
  }
}