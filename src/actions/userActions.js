export const FETCH_USERS_BEGIN = "FETCH_USERS_BEGIN";
export const FETCH_USERS_SUCCEED = "FETCH_USERS_SUCCEED";
export const FETCH_USERS_FAIL = "FETCH_USERS_FAIL";
export const SET_NAME_FILTER_VALUE = "SET_NAME_FILTER_VALUE";

export const fetchUsersBegin = () => (
  {
    type: FETCH_USERS_BEGIN
  }
)

export const fetchUsersSucceed = users => (
  {
    type: FETCH_USERS_SUCCEED,
    payload: users
  }
)

export const fetchUsersFail = error => (
  {
    type: FETCH_USERS_FAIL,
    payload: error
  }
)

export const setNameFilterValue = nameFilterValue => (
  {
    type: SET_NAME_FILTER_VALUE,
    payload: nameFilterValue
  }
)

export const fetchUsers = () => {
  return dispatch => {
    dispatch(fetchUsersBegin());
    return fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(data => dispatch(fetchUsersSucceed(data)))
        .catch(err => dispatch(fetchUsersFail(err)));
  }
}
