import React from 'react';
import { Menu, Icon } from 'antd';
import { connect } from 'react-redux';

const NavBar = (props) => {
  const { authenticationReducer } = props;
  
  console.log("render navbar")
  return (
    <Menu
      theme="dark"
      mode="horizontal"
      style={{ lineHeight: '64px', paddingLeft: '40px' }}
      selectedKeys={["-1"]}
    >
      <Menu.Item key="1" onClick={e => {
        props.history.push("/users");
      }}
      hidden={!authenticationReducer["isLoggedIn"]}
      >
        <Icon type="user" />Users
      </Menu.Item>
      <Menu.Item key="2" onClick={() => {
        props.history.push("/processLogout");
      }}
      hidden={!authenticationReducer["isLoggedIn"]}
      >
        <Icon type="logout" />Log out
      </Menu.Item>
    </Menu>
  );
}

const mapStateToProps = state => {
  return {
    authenticationReducer: state.authenticationReducer
  }
}

export default connect(mapStateToProps) (NavBar);