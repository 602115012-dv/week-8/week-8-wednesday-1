import React, { useEffect } from 'react';
import { Row, Col, Typography, Table, Input } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUsers, setNameFilterValue } from '../actions/userActions';

const { Column } = Table;

const UserList = (props) => { 
  const { 
    userReducer, 
    fetchUsers, 
    setNameFilterValue, 
    authenticationReducer 
  } = props;
  console.log(props)
  useEffect(() => {
    fetchUsers();
    setNameFilterValue("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if(!authenticationReducer["isLoggedIn"]) {
    props.history.push("/login");
  }

  const filter = users => {
    let filteredUsers = users;

    filteredUsers = filteredUsers.filter(filterUserByName);

    return filteredUsers;
  }

  const filterUserByName = user => {
    let filterValue = userReducer["nameFilterValue"].toUpperCase();
    return user.name.toUpperCase().includes(filterValue);
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">Users</Typography.Title>
          <Input 
            placeholder="Filter name" 
            onKeyUp={e => setNameFilterValue(e.target.value)} 
          />
          <Table 
            dataSource={filter(userReducer["users"])} 
            rowKey="id"
            loading={userReducer["loading"]}
          >
            <Column title="Id" dataIndex="id" />
            <Column title="Name" dataIndex="name" />
            <Column title="Email" dataIndex="email" />
            <Column title="Action" render={
              (data) => {
                return (
                  <span>
                    <a
                    onClick={() => props.history.push(`/users/${data.id}/todo`)}>
                      See todo list
                    </a>
                  </span>
                );
              }
            }/>
          </Table>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    authenticationReducer: state.authenticationReducer
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchUsers, setNameFilterValue } , dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (UserList);