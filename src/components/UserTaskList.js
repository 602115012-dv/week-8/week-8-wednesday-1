import React, { useEffect } from 'react';
import { Row, Col, Typography, Table, Tag, Button, Select, Input } from 'antd';
import TaskStatus from './TaskStatus';
import { 
  fetchUserTasks, 
  setTaskStatusFilterValue, 
  updateTasks,
  setTaskTitleFilterValue
} from '../actions/tasksAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const { Column } = Table;
const { Option } = Select;

const UserTaskList = (props) => {
  const { 
    taskReducer, 
    fetchUserTasks, 
    setTaskStatusFilterValue, 
    updateTasks,
    setTaskTitleFilterValue
  } = props;

  useEffect(() => {
    fetchUserTasks(props.match.params.userId);
    setTaskStatusFilterValue("All");
    setTaskTitleFilterValue("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderTitle = (data) => {
    return (
      <Typography.Text delete={data.completed}>{data.title}</Typography.Text>
    );
  }

  const renderStatus = (data) => {
    return (
      <Tag 
      color={data.completed ? "green" : "blue"}>
        {data.completed ? "Done" : "Doing"}
      </Tag>
    );
  }

  const updateTask = (task, taskStatus) => {
    let tempTasks = [...taskReducer["tasks"]];
    let taskIndex = tempTasks.findIndex((item) => {
      return item.id === task.id;
    });

    switch (taskStatus) {
      case TaskStatus.DONE:
        tempTasks[taskIndex].completed = true;
        break;
      case TaskStatus.DOING:
        tempTasks[taskIndex].completed = false;
        break;
      case TaskStatus.REMOVE:
        tempTasks.splice(taskIndex, 1);
        break;
      default:
        break;
    }

    updateTasks(tempTasks);
  }

  const renderButtons = (data) => {
    if (data.completed) {
      return (
        <span>
          <Button type="primary" onClick={() => updateTask(data, TaskStatus.DOING)}>Reopen</Button>
          <Button type="danger" onClick={() => updateTask(data, TaskStatus.REMOVE)}>Remove</Button>
        </span>
      );
    }
    return (
      <Button type="primary" onClick={() => updateTask(data, TaskStatus.DONE)}>Done</Button>
    );
  }

  const filter = (tasks) => {
    let filteredTasks = tasks;

    filteredTasks = filteredTasks.filter(filterTaskByTaskStatus);
    filteredTasks = filteredTasks.filter(filterTaskByTaskTitle);

    return filteredTasks;
  }

  const filterTaskByTaskStatus = task => {
    let value = taskReducer["taskStatusFilterValue"];
    if (value === "All") {
      return true;
    } else {
      let status = value === TaskStatus.DONE ? true : false;
      return task["completed"] === status;
    }
  }

  const filterTaskByTaskTitle = task => {
    let filterValue = taskReducer["taskTitleFilterValue"].toUpperCase();
    return task["title"].toUpperCase().includes(filterValue);
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">
            Task list
          </Typography.Title>
          <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => {
              setTaskStatusFilterValue(value);
            }}>
            <Option value="All">All</Option>
            <Option value={TaskStatus.DOING}>Doing</Option>
            <Option value={TaskStatus.DONE}>Done</Option>
          </Select>
          <Input 
            placeholder="Filter task title" 
            onKeyUp={e => setTaskTitleFilterValue(e.target.value)} 
          />
          <Table 
            dataSource={filter(taskReducer["tasks"])} 
            rowKey="id"
            loading={taskReducer["loading"]}
          >
            <Column title="Task id" dataIndex="id" />
            <Column title="Title" render={data => renderTitle(data)} />
            <Column title="Status" render={data => renderStatus(data)} />
            <Column title="Action" render={data => renderButtons(data)} />
          </Table>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    taskReducer: state.taskReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ 
      fetchUserTasks, 
      setTaskStatusFilterValue, 
      updateTasks,
      setTaskTitleFilterValue
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (UserTaskList);