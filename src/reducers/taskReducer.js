import {
  FETCH_TASKS_BEGIN,
  FETCH_TASKS_FAIL,
  FETCH_TASKS_SUCCEED,
  SET_TASK_STATUS_FILTER_VALUE,
  UPDATE_TASKS,
  SET_TASK_TITLE_FILTER_VALUE
} from '../actions/tasksAction';

const initialState = {
  userId: "",
  loading: false,
  error: "",
  tasks: [],
  taskTitleFilterValue: "",
  taskStatusFilterValue: "All"
}

export const taskReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TASKS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_TASKS_SUCCEED:
      return {
        ...state,
        loading: false,
        tasks: action.payload
      }
    case FETCH_TASKS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case UPDATE_TASKS:
      return {
        ...state,
        tasks: action.payload
      }
    case SET_TASK_STATUS_FILTER_VALUE:
      return {
        ...state,
        taskStatusFilterValue: action.payload
      }
    case SET_TASK_TITLE_FILTER_VALUE:
      return {
        ...state,
        taskTitleFilterValue: action.payload
      }
    default:
      return state;
  }
}