import {
  FETCH_USERS_BEGIN,
  FETCH_USERS_SUCCEED,
  FETCH_USERS_FAIL,
  SET_NAME_FILTER_VALUE
} from '../actions/userActions';


const initialState = {
  loading: false,
  error: "",
  users: [],
  nameFilterValue: ""
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_USERS_SUCCEED:
      return {
        ...state,
        loading: false,
        users: action.payload
      }
    case FETCH_USERS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case SET_NAME_FILTER_VALUE:
      return {
        ...state,
        nameFilterValue: action.payload
      }
    default:
      return state;
  }
}