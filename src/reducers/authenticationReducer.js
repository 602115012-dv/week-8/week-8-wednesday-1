import { LOG_IN, LOGOUT } from '../actions/authenticationActions';

const initialState = {
  isLoggedIn: false,
  username: '',
  password: ''
}

export const authenticationReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
      return {
        isLoggedIn: true,
        username: action["username"],
        password: action["password"]
      };
    case LOGOUT:
      return initialState;
    default:
      return state;
  }
}