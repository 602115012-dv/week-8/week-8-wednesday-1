import { combineReducers } from 'redux';
import { authenticationReducer } from './authenticationReducer';
import { userReducer } from './userReducer';
import { taskReducer } from './taskReducer';

const rootReducer = combineReducers({
  authenticationReducer,
  userReducer,
  taskReducer
});

export default rootReducer;